package almafa;
public class Alma {
    private String szin;
    private String iz;
    private Integer magSzam;
    private Boolean erett;
    
    public Alma(){
        this.erett = Boolean.FALSE; // = false
        this.iz = "Íztelen";
        this.magSzam = 0;
        this.szin = "Zöld";
    }
    
    public Alma(String iz, String szin, Integer magSzam, Boolean erett){
        this.erett = erett;
        this.iz = iz;
        if(magSzam >= 0) { this.magSzam = magSzam; }
        else { this.magSzam = 0; }
        this.szin = szin;
    }
    
    public String getIz(){
        return this.iz;
    }

    public String getSzin() {
        return this.szin;
    }

    public Integer getMagSzam() {
        return this.magSzam;
    }

    public Boolean isErett() {
        return this.erett;
    }
    
    public void setSzin(String szin){
        this.szin = szin;
    }
    
    public void pluszEgyMag(){
        //feltételezzük, hogy 10 magnál nem lehet több
        if(this.magSzam < 10) {
            this.magSzam++;
        }
    }
    
    public void pluszMag(Integer ujMagokSzama){
        if(this.magSzam + ujMagokSzama <= 10){
            this.magSzam += ujMagokSzama;
        }
    }
    
    public void megerett(){
        this.erett = Boolean.TRUE;
    }

    public void setIz(String iz) {
        this.iz = iz;
    }
    
    
    
    
}
